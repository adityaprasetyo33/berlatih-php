1.buat Database

create database myshop

2.Membuat Table

create table users(id integer auto_increment,name varchar(255),email varchar(255),password varchar(255),primary key (id));

create table items(id integer auto_increment,name varchar(255),description varchar(255),price integer,stock integer,category_id integer,primary key(id),foreign key(category_id) references categories(id));

create table categories(id integer auto_increment,name varchar(255),primary key(id));

3.Memasukkan Data

A. DATA USERS

INSERT INTO users (name, email, password) VALUES ('John Doe','john@doe.com','john123');

INSERT INTO users (name, email, password) VALUES ('Jane Doe','jane@doe.com','jenita123');

B. DATA ITEMS

INSERT INTO categories(name) VALUES ('gadget');

INSERT INTO categories(name) VALUES ('cloth');

INSERT INTO categories(name) VALUES ('men');

INSERT INTO categories(name) VALUES ('women');

INSERT INTO categories(name) VALUES ('branded');

C. DATA KATEGORI

INSERT INTO items(name, description, price, stock, category_id) VALUES ('Sumsang b50','hape keren dari merek sumsang',4000000,100,1);

INSERT INTO items(name, description, price, stock, category_id) VALUES ('Uniklooh','baju keren dari brand ternama',500000,50,2);

INSERT INTO items(name, description, price, stock, category_id) VALUES ('IMHO Watch','jam tangan anak yang jujur banget	',2000000,10,1);

4.Mengambil data

A. DATA USERS
SELECT id, name, email FROM users;

B. DATA ITEMS
SELECT * FROM items WHERE price > 1000000;

C. DATA ITEMS JOIN KATEGORI	
SELECT i.name, i.description,i.price,i.stock,i.category_id,c.name AS kategori FROM items i JOIN categories c ON i.category_id=c.id;

5.Mengubah Data

UPDATE items SET price=2500000 WHERE name LIKE '%sumsang%';