<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>

<body>
    <h1>Berlatih Array</h1>

    <?php
    echo "<h3> Soal 1 </h3>";
    /* 
            SOAL NO 1
            Kelompokkan nama-nama di bawah ini ke dalam Array.
            Kids : "Mike", "Dustin", "Will", "Lucas", "Max", "Eleven" 
            Adults: "Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"
        */
    $kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"]; // Lengkapi di sini
    $adults = ["Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"];
    echo "Kids: <br>";
    print_r($kids);
    echo "<br><br> Adults: <br>";
    print_r($adults);
    echo "<h3> Soal 2</h3>";
    /* 
            SOAL NO 2
            Tunjukkan panjang Array di Soal No 1 dan tampilkan isi dari masing-masing Array.
        */
    echo "Cast Stranger Things: ";
    echo "<br>";
    echo "Total Kids: " . count($kids); // Berapa panjang array kids
    echo "<br>";
    echo "<ol>";
    echo "<li> $kids[0] </li>";
    // Lanjutkan
    echo "<li> $kids[1] </li>";
    echo "<li> $kids[2] </li>";
    echo "<li> $kids[3] </li>";
    echo "<li> $kids[4] </li>";
    echo "<li> $kids[5] </li>";

    echo "</ol>";

    echo "Total Adults: " . count($adults); // Berapa panjang array adults
    echo "<br>";
    echo "<ol>";
    echo "<li> $adults[0] </li>";
    // Lanjutkan
    echo "<li> $adults[1] </li>";
    echo "<li> $adults[2] </li>";
    echo "<li> $adults[3] </li>";
    echo "<li> $adults[4] </li>";
    echo "</ol>";

    /*
            SOAL No 3
            Susun data-data berikut ke dalam bentuk Asosiatif Array (Array Multidimensi)
            
            Name: "Will Byers"
            Age: 12,
            Aliases: "Will the Wise"
            Status: "Alive"

            Name: "Mike Wheeler"
            Age: 12,
            Aliases: "Dungeon Master"
            Status: "Alive"

            Name: "Jim Hopper"
            Age: 43,
            Aliases: "Chief Hopper"
            Status: "Deceased"

            Name: "Eleven"
            Age: 12,
            Aliases: "El"
            Status: "Alive"
            
        */

    $biodata = [
        "Biodata 1" => array("Name" => "Will Byers", "Age" => 12, "Aliases" => "Will the Wise", "Status" => "Alive"),
        "Biodata 2" => array("Name" => "Mike Wheeler", "Age" => 12, "Aliases" => "Dungeon Master", "Status" => "Alive"),
        "Biodata 3" => array("Name" => "Jim Hopper", "Age" => 43, "Aliases" => "Chief Hopper", "Status" => "Deceased"),
        "Biodata 4" => array("Name" => "Eleven", "Age" => 12, "Aliases" => "El", "Status" => "Alive")
    ];

    echo "<h3>Soal 3</h3>";
    echo "Nama: " . $biodata["Biodata 1"]["Name"] . "<br>";
    echo "Age: " . $biodata["Biodata 1"]["Age"] . "<br>";
    echo "Aliases: " . $biodata["Biodata 1"]["Aliases"] . "<br>";
    echo "Status: " . $biodata["Biodata 1"]["Status"] . "<br><br>";

    echo "Nama: " . $biodata["Biodata 2"]["Name"] . "<br>";
    echo "Age: " . $biodata["Biodata 2"]["Age"] . "<br>";
    echo "Aliases: " . $biodata["Biodata 2"]["Aliases"] . "<br>";
    echo "Status: " . $biodata["Biodata 2"]["Status"] . "<br><br>";

    echo "Nama: " . $biodata["Biodata 3"]["Name"] . "<br>";
    echo "Age: " . $biodata["Biodata 3"]["Age"] . "<br>";
    echo "Aliases: " . $biodata["Biodata 3"]["Aliases"] . "<br>";
    echo "Status: " . $biodata["Biodata 3"]["Status"] . "<br><br>";

    echo "Nama: " . $biodata["Biodata 4"]["Name"] . "<br>";
    echo "Age: " . $biodata["Biodata 4"]["Age"] . "<br>";
    echo "Aliases: " . $biodata["Biodata 4"]["Aliases"] . "<br>";
    echo "Status: " . $biodata["Biodata 4"]["Status"] . "<br><br>";

    ?>
</body>

</html>